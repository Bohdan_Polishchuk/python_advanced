def fac(n):

    res = 1
    for i in range(1, n + 1):
        res *= i
    return res

def gcd(a, b):

    while b:
        a, b = b, a % b
    return a

def fib():

    a, b = 1, 1
    while True:
        yield a
        a, b = b, a + b

def flatten(seq):

    return sum(([x] if not isinstance(x, (list, tuple))
                else flatten(x) for x in seq), [])

class call_count:
    def __init__(self, func):
        self._func = 0

    def __call__(self, *args, **kwargs):
        self._func += 1
        return {'args': args, 'kwargs': kwargs}

    @property
    def call_count(self):
        res = {}
        if res:
            return {'args': args, 'kwargs': kwargs}
        return self._func